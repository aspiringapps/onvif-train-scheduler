import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { AgmCoreModule } from 'angular2-google-maps/core/core-module';
import { MyApp } from './app.component';
import { AdMobFree } from '@ionic-native/admob-free';
import { IonicStorageModule } from '@ionic/storage';
//import{JournalPage}from'../pages/journal/journal';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import{AngularFireModule} from'angularfire2';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import{MapsPage}from'../pages/maps/maps';
import{StreamPage}from'../pages/stream/stream';
import{CamerapreviewPage}from'../pages/camerapreview/camerapreview';
import {CameraPreview} from '@ionic-native/camera-preview';


import firebase from 'firebase';

import{RoutesPage}from'../pages/routes/routes';

 export const firebaseConfig ={
  apiKey: "AIzaSyCIdsIv_tHMA5F2DV4oZgN1gDwRWUaBsbc",
    authDomain: "madeeasyrough-1483977913225.firebaseapp.com",
    databaseURL: "https://madeeasyrough-1483977913225.firebaseio.com",
    projectId: "madeeasyrough-1483977913225",
    storageBucket: "madeeasyrough-1483977913225.appspot.com",
    messagingSenderId: "762082224219"
  
  };

  

  
@NgModule({
  declarations: [
    MyApp,
    
    HomePage,
    TabsPage,
    
    MapsPage,RoutesPage,
    StreamPage,
    CamerapreviewPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
     IonicStorageModule.forRoot(),
    AgmCoreModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
   
     
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
   
    HomePage,
    TabsPage,
    MapsPage,RoutesPage,
    StreamPage,CamerapreviewPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
     AdMobFree,
    AngularFireDatabase,
    AngularFireModule,
    CameraPreview,

    {provide: ErrorHandler, useClass: IonicErrorHandler},
   
  ]
})
export class AppModule {}
