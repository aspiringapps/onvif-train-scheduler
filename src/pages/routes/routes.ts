import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{FirebaseListObservable}from'angularfire2/database';
import{AngularFireDatabase}from'angularfire2/database';
import{AngularFireModule}from'angularfire2';


import firebase from 'firebase';



declare var google;



/**
 * Generated class for the RoutesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-routes',
  templateUrl: 'routes.html',
})
export class RoutesPage {
 
public userProfileRef:firebase.database.Reference;
  
  public tolist:Array<any>;
  public sourceStationName:any;

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  start = this.sourceStationName;
  end = this.sourceStationName;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  constructor(public navCtrl: NavController) {
     this.userProfileRef = firebase.database().ref('/');

  }

  	 getfromLists():firebase.database.Reference {

    return this.userProfileRef;
  }


  ionViewDidLoad(){



    this.initMap();

      {
       this.getfromLists().on('value', snapshot => {
      this.tolist = [];
      snapshot.forEach( snap => {
        this.tolist.push({
          id: snap.key,
                   StationName:snap.val().StationName,
         

        }); 
        this.sourceStationName=snap.val().sourceStationName;
     console.log(this.sourceStationName);

    
        return false
      });
       })
    
       
 
  
      }
  }

  initMap() {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 7,
      center: {lat: 20.2961,
			lng: 85.8245},
    });

    this.directionsDisplay.setMap(this.map);
  }

  calculateAndDisplayRoute() {
    this.directionsService.route({
      origin: this.start,
      destination: this.end,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        console.log(this.start);
        console.log(this.end);
        this.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

}


