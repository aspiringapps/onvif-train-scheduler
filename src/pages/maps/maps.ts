import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{FirebaseListObservable}from'angularfire2/database';
import{AngularFireDatabase}from'angularfire2/database';
import{AngularFireModule}from'angularfire2';


import firebase from 'firebase';


import { IMarker, IPoint } from './interfaces';
declare var google;

@IonicPage()
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})
export class MapsPage {

	@ViewChild('map') mapElement: ElementRef;
  map: any;
  
	public markers: IMarker[];
	public origin: IPoint;
	public zoom: number;
    


   public accountList: Array<any>;
 
public userProfileRef:firebase.database.Reference;

public tos:firebase.database.Reference;
public dates:firebase.database.Reference;
public datesList:Array<any>;

public tolist:Array<any>;

	constructor() {

		 this.userProfileRef = firebase.database().ref('from').child('/'); 
		this.initMarkers();
		this.origin = {
			lat: 20.2961,
			lng: 85.8245,
		};
		this.zoom = 8;
	}
	
  


 
	public clickedMarker(label: string) {
	//	window.alert(`clicked the marker: ${label || ''}`);
	}

	private initMarkers(): void {
		this.markers = [{
			lat: 20.2961,
			lng: 85.8245,
		//	label: 'Bhuwaneshwar'
		}, {
			lat: 19.3150,
			lng: 84.7941,
			//label: '2 Brahmapur'
		},{
			lat: 16.5062,
			lng: 80.6480,
			//label: 'Vijaywada'
		},
		{
			lat: 13.6366,
			lng: 79.5066,
//label: 'Renigunta Jn'
		},
		{
			lat: 12.5541,
			lng: 78.5718,
			//label: 'Jolarpetai'
		},
	
		{
			lat: 19.0213,
			lng: 72.8424,
			//label: 'Dadar'
		},
		{
			lat: 19.2183,
			lng: 72.9781,
			//label: 'Thane'
		},
		
		{
			lat: 19.6973,
			lng: 73.5609,
			//label: 'Igatpuri'
		},
		
		{
			lat: 19.9703,
			lng: 73.8301,
		//	label: 'Nasik ROad'
		},
		{
			lat: 21.0077,
			lng: 75.5626,
		//	label: 'Jalgaon'
		},
		{
			lat: 20.4641,
			lng: 74.9969,
		//	label: 'Chalisgaon'
		},
		{
			lat:21.0455,
			lng: 75.8011,
		//	label: 'Bhusava'
		},	
		{
			lat: 12.9716,
			lng:  77.5946,
			//label: '7 Bangalore'
		},

		
		{
			lat:20.7336,
			lng: 74.8916,
			//label: 'Shirud'
		},
			{
			lat:20.9042,
			lng: 74.7749,
			//label: 'Dhule'
		},
		
		
		 {
			lat: 17.6868,                  
			lng: 83.2185,
			//label: '3 Vishakapatnam'   
		}];
	}
}

